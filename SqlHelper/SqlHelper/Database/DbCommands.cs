﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SqlHelper
{
    public class DbCommands
    {
      
        public IDataReader Execute(string query, IDictionary<string, object> parameters = null, IDbConnection con = null)
        {
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    if (parameters != null)
                    {
                        foreach (var p in parameters)
                        {
                            var parameter = cmd.CreateParameter();

                            parameter.ParameterName = p.Key.Contains("@") ? p.Key : "@" + p.Key;
                            parameter.Value = p.Value;

                            cmd.Parameters.Add(parameter);
                        }
                    }

                    return cmd.ExecuteReader();
                }
        }

    }
}
