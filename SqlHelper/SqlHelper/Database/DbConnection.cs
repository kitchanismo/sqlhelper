﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace SqlHelper
{
   public class DbConnection : SqlBuilder
    {
        protected IDbConnection _con;
        protected IDataReader _reader;
        protected string _conString;

        protected IDbConnection Connection()
        {
            _con.Close();
            _con.ConnectionString = _conString;
            _con.Open();
          
            return _con;
        }
        
    }

   
}
