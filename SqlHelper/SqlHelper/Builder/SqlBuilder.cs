﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlHelper
{
    public class SqlBuilder : ISqlBuilder
    {

        public string _tableName { get; set; }
        public ICollection<string> _entityNames { get; set; }
        public string[] _excludeEntityNames { get; set; }
        public ICollection<string> _conditionalEntityNames { get; set; }

        private StringBuilder queryBuilder = new StringBuilder();

        public string InsertQuery()
        {
            string fieldBuilder = string.Empty;
            string valueBuilder = string.Empty;

            foreach (var entityName in _entityNames)
            {
                bool included = true;

                if (_excludeEntityNames != null)
                {
                    foreach (var excludeEntityName in _excludeEntityNames)
                    {
                        if (entityName.Contains(excludeEntityName))
                        {
                            included = false;
                            break;
                        }
                    }
                }

                if (included)
                {
                    fieldBuilder += entityName + ",";
                    valueBuilder += "@" + entityName + ",";
                }
            }

            fieldBuilder = fieldBuilder.TrimEnd(',');
            valueBuilder = valueBuilder.TrimEnd(',');

            queryBuilder.Clear();
            queryBuilder.AppendLine("INSERT INTO " + _tableName + " ( " + fieldBuilder + " )");
            queryBuilder.AppendLine("VALUES ( " + valueBuilder + " );");

            return Convert.ToString(queryBuilder);
        }

        public string UpdateQuery()
        {
            string valueBuilder = string.Empty;
            string conditionValueBuilder = string.Empty;

            foreach (var entityName in _entityNames)
            {
                bool included = true;

                if (_excludeEntityNames != null)
                {
                    foreach (var excludeEntityName in _excludeEntityNames)
                    {
                        if (entityName.Contains(excludeEntityName))
                        {
                            included = false;
                            break;
                        }
                    }
                }

                if (included)
                {
                    valueBuilder += entityName + " = @" + entityName + ",";
                }
            }

            if (_conditionalEntityNames != null)
            {
                foreach (var conditionalEntityName in _conditionalEntityNames)
                {
                    conditionValueBuilder += conditionalEntityName + " = " + "@c_" + conditionalEntityName + " AND ";
                }
            }

            valueBuilder = valueBuilder.TrimEnd(',');
            conditionValueBuilder = conditionValueBuilder.Substring(0, conditionValueBuilder.Length - 4);

            queryBuilder.Clear();
            queryBuilder.AppendLine("UPDATE " + _tableName + " SET " + valueBuilder);
            queryBuilder.AppendLine("WHERE " + conditionValueBuilder + ";");

            return Convert.ToString(queryBuilder);
        }

        public string DeleteQuery()
        {
            string conditionBuilder = string.Empty;

            foreach (var conditionalEntityName in _conditionalEntityNames)
            {
                conditionBuilder += conditionalEntityName + " = @" + conditionalEntityName + " AND ";
            }

            conditionBuilder = conditionBuilder.Substring(0, conditionBuilder.Length - 5);

            queryBuilder.Clear();
            queryBuilder.AppendLine("DELETE FROM " + _tableName);
            queryBuilder.AppendLine("WHERE " + conditionBuilder);

            return Convert.ToString(queryBuilder);
        }

        public string ReadAllQuery()
        {
            string conditionBuilder = string.Empty;

            if (_conditionalEntityNames != null)
            {
                foreach (var conditionalEntityName in _conditionalEntityNames)
                {
                    conditionBuilder += conditionalEntityName + " = @" + conditionalEntityName + " AND ";
                }
                conditionBuilder = conditionBuilder.Substring(0, conditionBuilder.Length - 5);
                queryBuilder.Clear();
                queryBuilder.AppendLine("SELECT " + getAllFields() + " FROM " + _tableName);
                queryBuilder.AppendLine(" WHERE " + conditionBuilder);
            }
            else
            {
                queryBuilder.Clear();
                queryBuilder.AppendLine("SELECT " + getAllFields() + " FROM " + _tableName);
            }
            return Convert.ToString(queryBuilder);
        }

        public string ReadOneQuery()
        {
            string conditionBuilder = string.Empty;
            foreach (var conditionalEntityName in _conditionalEntityNames)
            {
                conditionBuilder += conditionalEntityName + " = @" + conditionalEntityName + " AND ";
            }

            conditionBuilder = conditionBuilder.Substring(0, conditionBuilder.Length - 5);

            queryBuilder.Clear();
            queryBuilder.AppendLine("SELECT " + getAllFields() + " FROM " + _tableName);
            queryBuilder.AppendLine("WHERE " + conditionBuilder);
            
            return Convert.ToString(queryBuilder);

        }

        private string getAllFields()
        {
            string fieldBuilder = string.Empty;

            foreach (var entityName in _entityNames)
            {
                fieldBuilder += entityName + ",";
            }

            return fieldBuilder = fieldBuilder.TrimEnd(',');
        }
    }
}
