﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlHelper
{
    interface ISqlBuilder
    {

       string _tableName { get; set; }
       ICollection<string> _entityNames { get; set; }
       string[] _excludeEntityNames { get; set; }
       ICollection<string> _conditionalEntityNames { get; set; }
       string InsertQuery();
       string UpdateQuery();
       string DeleteQuery();
    }
}
