﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SqlHelper
{
    public class CRUD : DbConnection
    {

        DbCommands command;
        SqlBuilder builder;

        public CRUD(IDbConnection con)
        {
            _con = con;
            _conString = con.ConnectionString;
          
            command = new DbCommands();
            builder = new SqlBuilder();
        }

        public void Insert<T>(T entity, string[] excludeEntityNames = null)
        {
           
                //requirements for building a query
                builder._tableName          = typeof(T).Name;
                builder._entityNames        = GetProperties(entity).Keys;
                builder._excludeEntityNames = excludeEntityNames;

                //execute by passing the query and properties
                command.Execute(builder.InsertQuery(), GetProperties(entity), Connection());
          
        }

        public void BulkInsert<T>(IEnumerable<T> entities)
        {
                foreach(var entity in entities)
                {
                    Insert(entity);
                }
        }

        public void Update<T>(T entity, IDictionary<string, object> conditionalEntities, string[] excludeEntityNames = null)
        {
                var mergeProperties = GetProperties(entity);

                foreach(var conditionalEntity in conditionalEntities)
                {
                    mergeProperties.Add("c_" + conditionalEntity.Key, conditionalEntity.Value);
                }

                //requirements for building a query
                builder._tableName              = typeof(T).Name;
                builder._entityNames            = mergeProperties.Keys;
                builder._excludeEntityNames     = excludeEntityNames;
                builder._conditionalEntityNames = conditionalEntities.Keys;

                //execute by passing the query and properties
                command.Execute(builder.UpdateQuery(), mergeProperties, Connection());
        }

        public void Delete<T>(IDictionary<string, object> conditionalEntities)
        {
                builder._tableName              = typeof(T).Name;
                builder._conditionalEntityNames = conditionalEntities.Keys;

                command.Execute(builder.DeleteQuery(), conditionalEntities, Connection());
        }

        public List<T> GetAll<T>(IDictionary<string, object> source = null) where T : class, new()
        {
            var Object = new T();
            var ObjectType = Object.GetType();

            builder._tableName = typeof(T).Name;
            builder._entityNames = GetProperties(Object).Keys;
            builder._conditionalEntityNames = (source == null) ? null : source.Keys;

            var reader = (source == null) ? command.Execute(builder.ReadAllQuery(), null, Connection()) : command.Execute(builder.ReadAllQuery(), source, Connection());

            var lists = new List<T>();
           
            while (reader.Read())
            {
                var newEntity = new T();
                foreach (var item in GetProperties(Object).Keys)
                {
                    ObjectType
                             .GetProperty(item)
                             .SetValue(newEntity, reader[item], null);
                }
                lists.Add(newEntity);
            }

            return lists;
        }

        public T GetOne<T>(IDictionary<string, object> source) where T : class, new()
        {
            var Object = new T();
            var ObjectType = Object.GetType();

            builder._tableName = typeof(T).Name;
            builder._conditionalEntityNames = source.Keys;
            builder._entityNames = GetProperties(Object).Keys;

            var reader = command.Execute(builder.ReadOneQuery(), source, Connection());

            while (reader.Read())
            {
                foreach (var item in GetProperties(Object).Keys)
                {
                    ObjectType
                             .GetProperty(item)
                             .SetValue(Object, reader[item], null);
                }
            }

            return Object;
        }

        public List<T> Query<T>(string sql, IDictionary<string, object> source = null) where T : class, new()
        {
            var Object = new T();
            var ObjectType = Object.GetType();

            var reader = (source == null) ? command.Execute(sql, null, Connection()) : command.Execute(sql, source, Connection());
        
            var lists = new List<T>();
           
            while (reader.Read())
            {
                var newEntity = new T();
                foreach (var item in GetProperties(Object).Keys)
                {
                    ObjectType
                             .GetProperty(item)
                             .SetValue(newEntity, reader[item], null);
                }
                lists.Add(newEntity);
            }

            return lists;
            
        }


        private IDictionary<string, object> GetProperties<T>(T entity)
        {
            var fields = entity.GetType().GetProperties();
            var dictFields = new Dictionary<string, object>();

            foreach(var field in fields)
            {
                dictFields.Add(field.Name, field.GetValue(entity, null));
            }

            return dictFields;
        }
    }

     static class Extension
     {
         
     }
}
