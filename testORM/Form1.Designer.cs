﻿namespace testORM
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnInsert = new System.Windows.Forms.Button();
            this.BtnBulkInsert = new System.Windows.Forms.Button();
            this.BtnUpdate = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnGetAll = new System.Windows.Forms.Button();
            this.BtnGetOne = new System.Windows.Forms.Button();
            this.BtnQuery = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnInsert
            // 
            this.BtnInsert.BackColor = System.Drawing.Color.White;
            this.BtnInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnInsert.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.ForeColor = System.Drawing.Color.Teal;
            this.BtnInsert.Location = new System.Drawing.Point(65, 55);
            this.BtnInsert.Name = "BtnInsert";
            this.BtnInsert.Size = new System.Drawing.Size(100, 78);
            this.BtnInsert.TabIndex = 0;
            this.BtnInsert.Text = "Insert";
            this.BtnInsert.UseVisualStyleBackColor = false;
            this.BtnInsert.Click += new System.EventHandler(this.button1_Click);
            // 
            // BtnBulkInsert
            // 
            this.BtnBulkInsert.BackColor = System.Drawing.Color.White;
            this.BtnBulkInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBulkInsert.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBulkInsert.ForeColor = System.Drawing.Color.Teal;
            this.BtnBulkInsert.Location = new System.Drawing.Point(195, 55);
            this.BtnBulkInsert.Name = "BtnBulkInsert";
            this.BtnBulkInsert.Size = new System.Drawing.Size(100, 78);
            this.BtnBulkInsert.TabIndex = 1;
            this.BtnBulkInsert.Text = "BulkInsert";
            this.BtnBulkInsert.UseVisualStyleBackColor = false;
            this.BtnBulkInsert.Click += new System.EventHandler(this.button2_Click);
            // 
            // BtnUpdate
            // 
            this.BtnUpdate.BackColor = System.Drawing.Color.White;
            this.BtnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnUpdate.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpdate.ForeColor = System.Drawing.Color.Teal;
            this.BtnUpdate.Location = new System.Drawing.Point(65, 162);
            this.BtnUpdate.Name = "BtnUpdate";
            this.BtnUpdate.Size = new System.Drawing.Size(100, 78);
            this.BtnUpdate.TabIndex = 2;
            this.BtnUpdate.Text = "Update";
            this.BtnUpdate.UseVisualStyleBackColor = false;
            this.BtnUpdate.Click += new System.EventHandler(this.button3_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.BackColor = System.Drawing.Color.White;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.ForeColor = System.Drawing.Color.Teal;
            this.BtnDelete.Location = new System.Drawing.Point(195, 162);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(100, 78);
            this.BtnDelete.TabIndex = 3;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = false;
            this.BtnDelete.Click += new System.EventHandler(this.button4_Click);
            // 
            // BtnGetAll
            // 
            this.BtnGetAll.BackColor = System.Drawing.Color.White;
            this.BtnGetAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnGetAll.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGetAll.ForeColor = System.Drawing.Color.Teal;
            this.BtnGetAll.Location = new System.Drawing.Point(323, 55);
            this.BtnGetAll.Name = "BtnGetAll";
            this.BtnGetAll.Size = new System.Drawing.Size(100, 78);
            this.BtnGetAll.TabIndex = 4;
            this.BtnGetAll.Text = "GetAll";
            this.BtnGetAll.UseVisualStyleBackColor = false;
            this.BtnGetAll.Click += new System.EventHandler(this.button5_Click);
            // 
            // BtnGetOne
            // 
            this.BtnGetOne.BackColor = System.Drawing.Color.White;
            this.BtnGetOne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnGetOne.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGetOne.ForeColor = System.Drawing.Color.Teal;
            this.BtnGetOne.Location = new System.Drawing.Point(323, 162);
            this.BtnGetOne.Name = "BtnGetOne";
            this.BtnGetOne.Size = new System.Drawing.Size(100, 78);
            this.BtnGetOne.TabIndex = 5;
            this.BtnGetOne.Text = "GetOne";
            this.BtnGetOne.UseVisualStyleBackColor = false;
            this.BtnGetOne.Click += new System.EventHandler(this.button6_Click);
            // 
            // BtnQuery
            // 
            this.BtnQuery.BackColor = System.Drawing.Color.White;
            this.BtnQuery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnQuery.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnQuery.ForeColor = System.Drawing.Color.Teal;
            this.BtnQuery.Location = new System.Drawing.Point(65, 271);
            this.BtnQuery.Name = "BtnQuery";
            this.BtnQuery.Size = new System.Drawing.Size(358, 123);
            this.BtnQuery.TabIndex = 6;
            this.BtnQuery.Text = "Query";
            this.BtnQuery.UseVisualStyleBackColor = false;
            this.BtnQuery.Click += new System.EventHandler(this.button7_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(498, 428);
            this.Controls.Add(this.BtnQuery);
            this.Controls.Add(this.BtnGetOne);
            this.Controls.Add(this.BtnGetAll);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.BtnUpdate);
            this.Controls.Add(this.BtnBulkInsert);
            this.Controls.Add(this.BtnInsert);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnInsert;
        private System.Windows.Forms.Button BtnBulkInsert;
        private System.Windows.Forms.Button BtnUpdate;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnGetAll;
        private System.Windows.Forms.Button BtnGetOne;
        private System.Windows.Forms.Button BtnQuery;
    }
}

