﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SqlHelper;
using System.Data.SqlClient;
using System.Data.OleDb;
//using MySql.Data.MySqlClient;

namespace testORM
{
    public partial class Form1 : Form
    {
        IDbConnection connection;
        CRUD crud;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //created by rencil
            //updated by kitchanismo (DynamicConnection,GetAll,GetOne,Query)
            
            //for mssql db 
            connection = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\testORM.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");

            //for msaccess db 
            //connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\testORM.mdb;");

            //for mysql db 
            //connection = new MySqlConnection(@"SERVER = localhost; PORT = 80; USERID = root; PASSWORD = 123; DATABASE = test;");
        
            crud = new CRUD(connection);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var updateKey = new Dictionary<string, object>();
            updateKey.Add("id", 16);

            var products = new List<Product>();
            products.Add(new Product { pcode = "fr", pname = "mcfries", pprice = 50 });
            products.Add(new Product { pcode = "fsr", pname = "mcfloat", pprice = 200 });

            products[1].pname = "float";

            crud.Update<Product>(products[1], updateKey, new string[] { "id" });
            MessageBox.Show("updated");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var product = new Product { pcode = "sudd", pname = "sundae", pprice = 30 };
            crud.Insert<Product>(product);
            MessageBox.Show("inserted");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var products = new List<Product>();
            products.Add(new Product { pcode = "fr", pname = "mcfries", pprice = 50 });
            products.Add(new Product { pcode = "fl", pname = "mcfloat", pprice = 200 });

            crud.BulkInsert<Product>(products);
            MessageBox.Show("inserted " + products.Count());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var deleteKey = new Dictionary<string, object>();
            deleteKey.Add("id", 16);
            crud.Delete<Product>(deleteKey);
            MessageBox.Show("deleted");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //all list in products
            List<Product> products = crud.GetAll<Product>();
            
            //set condition, you can add more condition
            //var findKey = new Dictionary<string, object>();

            //findKey.Add("pname", "mcfries");

            //all list in products with filtering
            //List<Product> products = crud.GetAll<Product>(findKey);

            ShowList(products);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var findKey = new Dictionary<string, object>();
            
            //all fields can be find and get, not limited to id only
            findKey.Add("id", 15);
              
            //set another condition
            //findKey.Add("pname", "sundae");

            Product product = crud.GetOne<Product>(findKey);

            MessageBox.Show(product.pcode);
            MessageBox.Show(product.pname);
            MessageBox.Show(product.pprice.ToString());
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                //
                //select all list in products
                //

                //var products = (List<Product>)crud.Query<Product>("SELECT * FROM PRODUCT");
                //ShowList(products);

                //
                //you can set a parameters to select
                //

                //var parameters = new Dictionary<string, object>();
                //parameters.Add("@id", 15);
                //var products = (List<Product>)crud.Query<Product>("SELECT * FROM PRODUCT WHERE id = @id", parameters);
                //ShowList(products);

                //
                //you can execute an insert update or delete
                //

                //crud.Query<Product>("INSERT into PRODUCT (pcode,pname,pprice) values ('f','y',5)");
                //MessageBox.Show("executed");

                //
                //you can set a parameters to execute
                //

                var parameters = new Dictionary<string, object>();
                parameters.Add("@pcode", "testing");
                parameters.Add("@pname", "testing");
                parameters.Add("@pprice", 105);

                crud.Query<Product>("INSERT into PRODUCT (pcode,pname,pprice) values (@pcode,@pname,@pprice)", parameters);
                MessageBox.Show("executed");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         
        }

        private void ShowList(List<Product>  products)
        {
          for (int i = 0; i < products.Count(); i++)
                {
                    MessageBox.Show(products[i].pcode);
                    MessageBox.Show(products[i].pname);
                    MessageBox.Show("" + products[i].pprice);
                }
        }
    }
     
    class Product
    {
        public string pname { get; set; }
        public string pcode { get; set; }
        public int pprice { get; set; }
    }
}
